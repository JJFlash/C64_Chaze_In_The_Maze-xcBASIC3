# Chaze in the Maze (Or... Digital Tag)
A [XC-BASIC3](https://xc-basic.net/) demo where you control a white asterisk, the Computer controls a little red face, and both of you are placed in a maze. Oh, and the Computer chases you :smiley:

![DigitalTag](DigitalTag3.gif)

## Features
- Wall following
- Main trick is Walter D. Pullen's Chain Algorithm, from his Daedalus program (see the [Algorithms](https://www.astrolog.org/labyrnth/algrithm.htm#solve) page, and the [Daedalus](https://www.astrolog.org/labyrnth/daedalus.htm) page)
- Use of Bresenham's Line Algorithm (taken almost verbatim from the [Wikipedia](https://en.wikipedia.org/wiki/Bresenham's_line_algorithm#All_cases) page)
- Uncomment the DEBUG lines to have a hint at how the path is roughly found 

## Requirements
- The [XC-BASIC3](https://xc-basic.net/) compiler, basically :grin:
- If you just want to try the demo, there's a [D64 file](DigitalTag3.d64) you can throw straight at your favourite emulator, or even the real thing!

## Post-mortem notes
If I see some interest in this project I'll add some production notes regarding this little program of mine (originally made on [FreeBASIC](https://freebasic.net/), where I spent two months and a half, in the spare time, later converted to XC-BASIC 2 and now to XC-BASIC 3).

Right now the source is not that commented (I wanted to see this online ASAP), but I'll probably add proper comments later :slightly_smiling_face:  

Some maps I drew "by hand" by using Notepad++ and then removing all the CRLF characters and inserting the start address ($0400) at the beginning of the map file.
MAZE and CAVERN were created and adapted with the Daedalus program, while SLOTRACE comes from an old Atari 2600 game, **Slot Racers**.