'MEMCPY 2, $C000, $8D
 
Const HIGHEST_Y = 24 : Const HIGHEST_X = 39
Const SCREEN_ADDR = 1024
Const WALL = 160 : Const SPACE = 32 : Const MOUSE = 81

Dim ScreenPosition as WORD fast : Dim NEWScreenPosition as WORD fast

Dim NextDirection as BYTE fast: Dim FirstNextDirection as BYTE fast
Dim Steps as WORD fast: Dim MaxSteps as WORD fast

Dim FinalScrPosition as WORD fast

Dim scrAddrCache(25) as WORD @loc_scrAddrCache
loc_scrAddrCache:
DATA AS WORD 1024, 1064, 1104, 1144, 1184, 1224, 1264, 1304, 1344, 1384
DATA AS WORD 1424, 1464, 1504, 1544, 1584, 1624, 1664, 1704, 1744, 1784
DATA AS WORD 1824, 1864, 1904, 1944, 1984

Dim DirArray(4) as INT @loc_DirArray
loc_DirArray:
DATA AS INT 2, -80, -2, 80

function random as byte (a as byte, b as byte) static
    poke $2A7, a
    poke $2A8, b
asm 
    lda #$ff
    sta $d40e
    sta $d40f
    lda #$80
    sta $d412
rand:
    lda $d41b
    cmp $2A8
    bcs rand
    adc $2A7
    sta $2A9
end asm
    random = peek($2A9)
end function

print "press a key (i need randomness)"
poke 198, 0: wait 198, 1

poke 53280,0 : poke 53281,0

restart:
memset SCREEN_ADDR, 1000, WALL
memset SCREEN_ADDR, 40, SPACE
memset scrAddrCache(24), 40, SPACE

FOR RowIndex As BYTE = 1 to 23
	'No SPACEing at the column 0!
	Poke scrAddrCache(RowIndex) + HIGHEST_X, SPACE
Next RowIndex

Steps = 0 : MaxSteps = 0 : FinalScrPosition = 0

'REM ScreenPosition = SCREEN_ADDR + 81 + 80 * CINT(10 * RND() ) + 2 * CINT(10 * RND() )
ScreenPosition = SCREEN_ADDR + 81 + (80 * cint(Random (0, 10))) + 2 * Random (0, 10) 'CINT *** VERY IMPORTANT *** 

poke ScreenPosition, 4 ' "D"

DigAnotherLocation:
	NextDirection = Random (0, 4) : FirstNextDirection = NextDirection
	if Steps > MaxSteps then MaxSteps = Steps: FinalScrPosition = NEWScreenPosition

TryDiggingAgain:
	NEWScreenPosition = ScreenPosition + DirArray(NextDirection)
	If Peek(NEWScreenPosition) = WALL Then
		Poke NEWScreenPosition, NextDirection
		Poke ScreenPosition + SHR(DirArray(NextDirection), 1), SPACE
		ScreenPosition = NEWScreenPosition
		Steps = Steps + 1
		Goto DigAnotherLocation
	End If

	NextDirection = (NextDirection + 1) AND 3 'Keep NextDirection between 0 and 3
	If NextDirection <> FirstNextDirection then Goto TryDiggingAgain
	
	NextDirection = Peek(ScreenPosition) : Poke ScreenPosition, SPACE
	Steps = Steps - 1
	If NextDirection < 4 then
		ScreenPosition = ScreenPosition - DirArray(NextDirection) 'Backtrack!
		Goto DigAnotherLocation
	end If
	
	Poke ScreenPosition, 1 : Poke FinalScrPosition, 2
	Print "{HOME} max steps: " ; MaxSteps ; " - final loc: " ; FinalScrPosition
	
'	****MOUSE****
	Poke ScreenPosition, MOUSE : NextDirection = 2
Mouse:
	wait $d011, 128 : wait $d011, 128, 128 : wait $d011, 128
	NEWScreenPosition = ScreenPosition + SHR(DirArray(NextDirection), 1)
	if NEWScreenPosition = FinalScrPosition then goto restart
	If Peek(NEWScreenPosition) = SPACE then
		Poke NEWScreenPosition, MOUSE : Poke ScreenPosition, SPACE
		ScreenPosition = NEWScreenPosition
		NextDirection = (NextDirection - 2) AND 3
	End If
	NextDirection = (NextDirection - 1) AND 3
	goto Mouse

'MEMCPY $C000, 2, $8D
